package cse_du.thesis.mtb.input_generator.input_processor;

import cse_du.thesis.mtb.input_generator.input_processor.domain.Graph;
import cse_du.thesis.mtb.input_generator.input_set_gen.domain.Node;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by User on 1/4/14.
 */
public class ScheduleProcessor {

    private static ArrayList<Graph> inputGraphSet;
    private static File file = null;
    private static Scanner fileScanner = null;
    private static int scheduleBits;

    public static void processSchedule() {
        inputGraphSet = InputProcessor.getInputGraphSet();
        int graphIterator = 0;
        try {
            file = new File("input_schedule.txt");
            fileScanner = new Scanner(file);
            while (fileScanner.hasNext()) {
                String line = fileScanner.nextLine();
                if (line.equals("SCHEDULE")) {
                    while (!line.equals("END")) {
                        line = fileScanner.nextLine();
                        String[] bits = line.split(" ");
                        scheduleBits = bits.length;
                        Graph tmpGraph = inputGraphSet.get(graphIterator);
                        ArrayList<Node> vertexList = tmpGraph.getVertexList();
                        for (Node node : vertexList) {
                            if (!line.equals("END")) {
                                node.setWakeUpSchedule(line.replaceAll("\\s",""));
                                line = fileScanner.nextLine();
                            }
                        }
                    }
                    graphIterator++;
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void printSchedule() {
        System.out.println("SCHEDULE BITS : " + scheduleBits);
        for (Graph graph : inputGraphSet) {

            ArrayList<Node> vertexList = graph.getVertexList();
            for (Node node : vertexList) {
                System.out.println(node.getWakeUpSchedule());
            }
            System.out.println();
        }
    }

    public static int getScheduleBits() {
        return scheduleBits;
    }
}
