package cse_du.thesis.mtb.input_generator.input_processor.domain;

import cse_du.thesis.mtb.input_generator.input_set_gen.domain.Edge;
import cse_du.thesis.mtb.input_generator.input_set_gen.domain.Node;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by User on 1/4/14.
 */
public class Graph {
    private int sourceNode;
    private int transmissionRange;
    private int numberOfNode;
    private ArrayList<Edge> edgeList;
    private ArrayList<Node> vertexList;
    private HashMap<Integer, HashSet<Integer>> adjacentList;

    public int getSourceNode() {
        return sourceNode;
    }

    public void setSourceNode(int sourceNode) {
        this.sourceNode = sourceNode;
    }

    public int getTransmissionRange() {
        return transmissionRange;
    }

    public void setTransmissionRange(int transmissionRange) {
        this.transmissionRange = transmissionRange;
    }

    public int getNumberOfNode() {
        return numberOfNode;
    }

    public void setNumberOfNode(int numberOfNode) {
        this.numberOfNode = numberOfNode;
    }

    public ArrayList<Edge> getEdgeList() {
        return edgeList;
    }

    public void setEdgeList(ArrayList<Edge> edgeList) {
        this.edgeList = edgeList;
    }

    public ArrayList<Node> getVertexList() {
        return vertexList;
    }

    public void setVertexList(ArrayList<Node> vertexList) {
        this.vertexList = vertexList;
    }

    public HashMap<Integer, HashSet<Integer>> getAdjacentList() {
        return adjacentList;
    }

    public void setAdjacentList(HashMap<Integer, HashSet<Integer>> adjacentList) {
        this.adjacentList = adjacentList;
    }
}
