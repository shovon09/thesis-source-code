package cse_du.thesis.mtb.input_generator.input_processor;

import cse_du.thesis.mtb.input_generator.input_processor.domain.Graph;
import cse_du.thesis.mtb.input_generator.input_set_gen.domain.Edge;
import cse_du.thesis.mtb.input_generator.input_set_gen.domain.Node;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

/**
 * Created by User on 1/4/14.
 */
public class InputProcessor {

    private static ArrayList<Edge> edgeListForBFS;
    private static int sourceNode;
    private static int transmissionRange;
    private static int numberOfNode;
    private static ArrayList<Edge> edgeList;
    private static ArrayList<Node> vertexList;
    private static HashMap<Integer, HashSet<Integer>> adjacentList = new HashMap<Integer, HashSet<Integer>>();
    private static int xNode = -1;
    private static int yNode = -2;
    private static HashSet<Integer> tmpSet;

    private static ArrayList<Graph> inputGraphSet = new ArrayList<Graph>();
    private static File file = null;
    private static Scanner fileScanner = null;

    public static void main(String[] args) {
        run();
    }

    public static void run() {
        processInputFile();
        printGraph();

        ScheduleProcessor.processSchedule();
        ScheduleProcessor.printSchedule();

        makeAdjacentList();
        printAdjacent();
    }

    public static void processInputFile() {

        try {
            file = new File("input_set.txt");
            fileScanner = new Scanner(file);
            while (fileScanner.hasNext()) {
                String line = fileScanner.nextLine();
                if (line.equals("GRAPH")) {

                    edgeList = new ArrayList<Edge>();
                    vertexList = new ArrayList<Node>();

                    line = fileScanner.nextLine();
                    sourceNode = Integer.parseInt(line);
                    line = fileScanner.nextLine();
                    numberOfNode = Integer.parseInt(line);
                    line = fileScanner.nextLine();
                    transmissionRange = Integer.parseInt(line);
                    for (int i = 1; i <= numberOfNode; i++) {
                        line = fileScanner.nextLine();
                        vertexList.add(new Node(i));
                    }
                    while (!line.equals("END")) {
                        line = fileScanner.nextLine();
                        if (!line.equals("END")) {
                            String[] edges = line.split("->");
                            Edge edge = new Edge();
                            edge.setX(Integer.parseInt(edges[0]));
                            edge.setY(Integer.parseInt(edges[1]));
                            edgeList.add(edge);
                        }
                    }
                }
                Graph graph = new Graph();
                graph.setSourceNode(sourceNode);
                graph.setNumberOfNode(numberOfNode);
                graph.setTransmissionRange(transmissionRange);
                graph.setVertexList(vertexList);
                graph.setEdgeList(edgeList);
                inputGraphSet.add(graph);

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    public static void printGraph() {
        for (Graph graph : inputGraphSet) {
            System.out.println(graph.getSourceNode());
            System.out.println(graph.getNumberOfNode());
            System.out.println(graph.getTransmissionRange());
            ArrayList<Node> vertex = graph.getVertexList();
            System.out.println(vertex);
            ArrayList<Edge> edgeList = graph.getEdgeList();
            for (Edge edge : edgeList) {
                System.out.println(edge.getX() + "->" + edge.getY());
            }

            System.out.println();
        }
    }

    public static ArrayList<Graph> getInputGraphSet() {
        return inputGraphSet;
    }

    public static int getScheduleBits() {
        return ScheduleProcessor.getScheduleBits();
    }

    private static void makeAdjacentList() {
        for (Graph graph : inputGraphSet) {

            ArrayList<Edge> tmpEdgeList = graph.getEdgeList();

            for (Edge edge : tmpEdgeList) {
                int tmpXNode = edge.getX();
                int tmpYNode = edge.getY();

                if (xNode == tmpXNode) {
                    xNode = tmpXNode;
                    yNode = tmpYNode;
                    tmpSet.add(yNode);
                } else {
                    xNode = tmpXNode;
                    yNode = tmpYNode;
                    tmpSet = new HashSet<Integer>();
                    tmpSet.add(yNode);
                    adjacentList.put(xNode, tmpSet);
                }
            }
            graph.setAdjacentList(adjacentList);
            adjacentList = new HashMap<Integer, HashSet<Integer>>();
        }

    }

    public static void printAdjacent() {
        System.out.println("ADJACENT LIST");
        for (Graph graph : inputGraphSet) {
            HashMap<Integer, HashSet<Integer>> adjacentList = graph.getAdjacentList();

            for (Map.Entry<Integer, HashSet<Integer>> entry : adjacentList.entrySet()) {
                int key = entry.getKey();
                HashSet values = entry.getValue();
                System.out.print(key + " ->  ");

                Iterator iterator = values.iterator();
                while (iterator.hasNext()) {
                    System.out.print(iterator.next() + "  ");
                }
                System.out.println();
            }
            System.out.println();
        }

    }
}
