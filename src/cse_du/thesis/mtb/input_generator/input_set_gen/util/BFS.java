package cse_du.thesis.mtb.input_generator.input_set_gen.util;

import cse_du.thesis.mtb.input_generator.input_set_gen.domain.Node;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Rifat
 * Date: 11/2/13
 * Time: 11:35 AM
 * To change this template use File | Settings | File Templates.
 */
public class BFS {
    private static final int WHITE = 1;
    private static final int GRAY = 2;
    private static final int BLACK = 3;
    private static final int INF = 999999999;
    private ArrayList<Node> vertexList = new ArrayList<Node>();
    private HashMap<Integer, HashSet<Integer>> adjacentList = new HashMap<Integer, HashSet<Integer>>();
    private Node sourceNode;
    private Queue<Node> queue = new LinkedList<Node>();
    private Node tmpAdjNode;

    public BFS(ArrayList<Node> vertexList, HashMap<Integer, HashSet<Integer>> adjacentList, Node sourceNode) {
        this.vertexList = vertexList;
        this.adjacentList = adjacentList;
        this.sourceNode = sourceNode;
        runBFS();
    }

    public void runBFS() {
        System.out.println("Running BFS for Checking Connectedness of Graph");
        for (Node node : vertexList) {
            if (node.equals(sourceNode)) {
                continue;
            } else {
                node.setNodeColor(WHITE);
                node.setDistance(INF);
                node.setPrevNode(null);
            }
        }
        sourceNode.setNodeColor(GRAY);
        sourceNode.setDistance(0);
        sourceNode.setPrevNode(null);
        queue.add(sourceNode);

        while (!queue.isEmpty()) {
            Node tmpNode = queue.remove();
            int nodeNum = tmpNode.getNodeNumber();
            System.out.println("From Node  : " + nodeNum);
            HashSet<Integer> adjSet = adjacentList.get(nodeNum);
            if (adjSet != null) {
                Iterator iterator = adjSet.iterator();
                while (iterator.hasNext()) {
                    int index = (Integer) iterator.next();
                    tmpAdjNode = vertexList.get(index - 1);
                    if (tmpAdjNode.getNodeColor() == WHITE) {
                        tmpAdjNode.setNodeColor(GRAY);
                        tmpAdjNode.setDistance(tmpNode.getDistance() + 1);
                        tmpAdjNode.setPrevNode(tmpNode);
                        queue.add(tmpAdjNode);
                        System.out.print(tmpAdjNode.getNodeNumber() + " ");
                    }
                }
            }
            System.out.println("\nDone");
            tmpNode.setNodeColor(BLACK);
        }

    }

    public boolean isGraphConnected() {
        for (Node node : vertexList) {
            if (node.getNodeColor() == WHITE) {
                return false;
            }
        }
        return true;
    }

}
