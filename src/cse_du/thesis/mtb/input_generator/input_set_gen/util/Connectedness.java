package cse_du.thesis.mtb.input_generator.input_set_gen.util;

import cse_du.thesis.mtb.input_generator.input_set_gen.domain.Edge;
import cse_du.thesis.mtb.input_generator.input_set_gen.domain.Node;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Rifatul
 * Date: 10/28/13
 * Time: 10:54 AM
 * BFS algorithm to test graph connectedness .
 */
public class Connectedness {
    private HashMap<Integer, HashSet<Integer>> adjacentList;
    private ArrayList<Edge> edgeList;
    private Node sourceNode;
    private int xNode = -1;
    private int yNode = -2;
    private HashSet<Integer> tmpSet;
    private ArrayList<Node> vertexList;


    public Connectedness(ArrayList<Node> vertexList, ArrayList<Edge> edgeList, int sourceNodeNumber) {
        adjacentList = new HashMap<Integer, HashSet<Integer>>();
        this.vertexList = vertexList;
        this.edgeList = edgeList;
        sourceNode = new Node(sourceNodeNumber);
        makeAdjacentList();
    }

    private void makeAdjacentList() {
        for (Edge edge : edgeList) {
            int tmpXNode = edge.getX();
            int tmpYNode = edge.getY();

            if (xNode == tmpXNode) {
                xNode = tmpXNode;
                yNode = tmpYNode;
                tmpSet.add(yNode);
            } else {
                xNode = tmpXNode;
                yNode = tmpYNode;
                tmpSet = new HashSet<Integer>();
                tmpSet.add(yNode);
                adjacentList.put(xNode, tmpSet);
            }
        }
    }

    public void printAdjacent() {
        System.out.println("ADJACENT LIST");

        for (Map.Entry<Integer, HashSet<Integer>> entry : adjacentList.entrySet()) {
            int key = entry.getKey();
            HashSet values = entry.getValue();
            System.out.print(key + " ->  ");

            Iterator iterator = values.iterator();
            while (iterator.hasNext()) {
                System.out.print(iterator.next() + "  ");
            }
            System.out.println();
        }
    }

    public boolean isGraphConnected() {

        BFS bfs = new BFS(vertexList, adjacentList, sourceNode);
        return bfs.isGraphConnected();
    }

    public HashMap<Integer, HashSet<Integer>> getAdjacentList() {
        return adjacentList;
    }
}
