package cse_du.thesis.mtb.input_generator.input_set_gen.domain;

/**
 * Created with IntelliJ IDEA.
 * User: Rifatul
 * Date: 10/4/13
 * Time: 10:04 PM
 * To change this template use File | Settings | File Templates.
 */
public class Edge {
    int x;
    int y;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
