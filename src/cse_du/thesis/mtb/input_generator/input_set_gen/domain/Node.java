package cse_du.thesis.mtb.input_generator.input_set_gen.domain;

/**
 * Created with IntelliJ IDEA.
 * User: Rifatul
 * Date: 10/29/13
 * Time: 11:00 AM
 * To change this template use File | Settings | File Templates.
 */
public class Node {
    private int nodeNumber;
    private int nodeColor;
    private int distance;
    private Node prevNode;
    private String wakeUpSchedule;

    public String getWakeUpSchedule() {
        return wakeUpSchedule;
    }

    public void setWakeUpSchedule(String wakeUpSchedule) {
        this.wakeUpSchedule = wakeUpSchedule;
    }

    public Node(int nodeNumber) {
        this.nodeNumber = nodeNumber;
    }

    public int getNodeNumber() {
        return nodeNumber;
    }


    public void setNodeNumber(int nodeNumber) {
        this.nodeNumber = nodeNumber;
    }

    public int getNodeColor() {
        return nodeColor;
    }

    public void setNodeColor(int nodeColor) {
        this.nodeColor = nodeColor;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public Node getPrevNode() {
        return prevNode;
    }

    public void setPrevNode(Node prevNode) {
        this.prevNode = prevNode;
    }

    @Override
    public boolean equals(Object obj) {
        Node node = (Node) obj;
        if (this.getNodeNumber() == node.getNodeNumber()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return "" + nodeNumber;

    }


}
