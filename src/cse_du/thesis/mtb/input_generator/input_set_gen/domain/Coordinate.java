package cse_du.thesis.mtb.input_generator.input_set_gen.domain;

/**
 * Created with IntelliJ IDEA.
 * User: Rifatul
 * Date: 10/4/13
 * Time: 7:56 PM
 * To change this template use File | Settings | File Templates.
 */
public class Coordinate {
    private int xCoordinate;
    private int yCoordinate;

    public int getxCoordinate() {
        return xCoordinate;
    }

    public void setxCoordinate(int xCoordinate) {
        this.xCoordinate = xCoordinate;
    }

    public int getyCoordinate() {
        return yCoordinate;
    }

    public void setyCoordinate(int yCoordinate) {
        this.yCoordinate = yCoordinate;
    }
}
