package cse_du.thesis.mtb.input_generator.input_set_gen;

import cse_du.thesis.mtb.input_generator.input_set_gen.domain.Coordinate;
import cse_du.thesis.mtb.input_generator.input_set_gen.domain.Edge;
import cse_du.thesis.mtb.input_generator.input_set_gen.domain.Node;
import cse_du.thesis.mtb.input_generator.input_set_gen.util.Connectedness;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class InputSetGenerator {
    private static final double PI = Math.PI;
    private static final int MAX_NODE = 50;
    private static final int COORDINATED_RANGE = 200;
    private static final int MAX_NODE_IN_TRANSMISSION_RANGE = 8;
    private static final int NUMBER_OF_SCHEDULE_BITS = 5;
    private static ArrayList<Edge> edgeListForBFS;
    private static int sourceNode;
    private static int transmissionRange;
    private static int numberOfNode;
    private static SecureRandom randomNumber = null;
    private static ArrayList<Coordinate> nodeCoordinates;
    private static ArrayList<Edge> edgeList;
    private static ArrayList<Node> vertexList;
    private static File file = null;
    private static File scheduleFile = null;
    private static FileWriter fileWriter = null;
    private static PrintWriter writer = null;
    private static FileWriter scheduleWriter = null;
    private static PrintWriter scheduler = null;
    private static Connectedness graphConnect;

    public InputSetGenerator() {

    }

    public static int generateInputSet(int doneInput) throws IOException {

        file = new File("input_set.txt");
        scheduleFile = new File("input_schedule.txt");
        fileWriter = new FileWriter(file, true);
        writer = new PrintWriter(fileWriter);
        scheduleWriter = new FileWriter(scheduleFile, true);
        scheduler = new PrintWriter(scheduleWriter);

        randomNumber = new SecureRandom();
        edgeListForBFS = new ArrayList<Edge>();
        nodeCoordinates = new ArrayList<Coordinate>();
        edgeList = new ArrayList<Edge>();
        vertexList = new ArrayList<Node>();

        numberOfNode = randomNumber.nextInt(MAX_NODE) + 4;
        System.out.println("Number of Node : " + numberOfNode);  // print to console for test/

        double tmpRange = Math.sqrt((MAX_NODE_IN_TRANSMISSION_RANGE * COORDINATED_RANGE * COORDINATED_RANGE) /
                (numberOfNode * PI));
        transmissionRange = (int) Math.round(tmpRange);
        System.out.println("Transmission Range is : " + transmissionRange); // print to console for test/

        sourceNode = randomNumber.nextInt(numberOfNode) + 1;
        System.out.println("Source Node :" + sourceNode);   // print to console for test/

        // The generator ///
        generateCoordinatePoint(numberOfNode);
        generateEdge();

        graphConnect = new Connectedness(vertexList, edgeListForBFS, sourceNode);
        graphConnect.printAdjacent();

        if (graphConnect.isGraphConnected()) {
            System.out.println("Result of BFS : Graph is connected");
            printInputSet();
            schedulerGenerator();
            doneInput++;
            System.out.println("Number of Graph : " + doneInput);

        } else {
            System.out.println("Result of BFS : Graph is not connected");

        }
        printWakeUp();

        return doneInput;
    }

    private static void generateCoordinatePoint(int numberOfNode) {

        for (int i = 0; i < numberOfNode; i++) {
            int xCoordinate = randomNumber.nextInt(COORDINATED_RANGE) + 1;
            int yCoordinate = randomNumber.nextInt(COORDINATED_RANGE) + 1;

            Coordinate nodeCoordinate = new Coordinate();
            nodeCoordinate.setxCoordinate(xCoordinate);
            nodeCoordinate.setyCoordinate(yCoordinate);
            nodeCoordinates.add(nodeCoordinate);

            System.out.println((i + 1) + ". x = " + xCoordinate + " y= " + yCoordinate);
            vertexList.add(new Node(i + 1));

        }
    }

    private static void generateEdge() {
        for (int i = 0; i < nodeCoordinates.size(); i++) {
            Coordinate currentNode = nodeCoordinates.get(i);
            for (int j = 0; j < nodeCoordinates.size(); j++) {
                if (i != j) {
                    Coordinate otherNode = nodeCoordinates.get(j);
                    int lowestXValue = currentNode.getxCoordinate() - transmissionRange;
                    int highestXValue = currentNode.getxCoordinate() + transmissionRange;
                    int lowestYValue = currentNode.getyCoordinate() - transmissionRange;
                    int highestYVale = currentNode.getyCoordinate() + transmissionRange;

                    int otherNodeXValue = otherNode.getxCoordinate();
                    int otherNodeYValue = otherNode.getyCoordinate();

                    if (otherNodeXValue >= lowestXValue && otherNodeXValue <= highestXValue) {
                        if (otherNodeYValue >= lowestYValue && otherNodeYValue <= highestYVale) {

                            Edge edgeTobeAdded = new Edge();
                            edgeTobeAdded.setX(i + 1);
                            edgeTobeAdded.setY(j + 1);
                            edgeList.add(edgeTobeAdded);
                            edgeListForBFS.add(edgeTobeAdded);
                            for (int k = 0; k < edgeList.size(); k++) {
                                Edge tmpEdge = edgeList.get(k);
                                if (tmpEdge.getX() == edgeTobeAdded.getY() && tmpEdge.getY() == edgeTobeAdded.getX()) {
                                    edgeList.remove(edgeTobeAdded);
                                }
                            }
                        }
                    }
                }

            }
        }
    }

    private static void printInputSet() {
        writer.append("GRAPH" + "\n");
        writer.append("" + sourceNode + "\n");
        writer.append("" + numberOfNode + "\n");
        writer.append("" + transmissionRange + "\n");
        int i = 1;
        for (Coordinate coordinate : nodeCoordinates) {
            writer.append(i + ". " + coordinate.getxCoordinate() + " " + coordinate.getyCoordinate() + "\n");
            i++;
        }
        for (Edge edge : edgeListForBFS) { //TODO : Change it to edgeList
            writer.append(edge.getX() + "->" + edge.getY() + "\n");
        }
        writer.append("END" + "\n");
        writer.close();

    }

    private static void schedulerGenerator() throws IOException {

        scheduler.append("SCHEDULE" + "\n");
        int randomBitPosition;
        for (int i = 0; i < numberOfNode; i++) {
            Node tmpNode = vertexList.get(i);
            String wakeUpSchedule = "";
            randomBitPosition = randomNumber.nextInt(NUMBER_OF_SCHEDULE_BITS);
            for (int j = 0; j < NUMBER_OF_SCHEDULE_BITS; j++) {
                if (j == randomBitPosition) {
                    scheduler.append(1 + " ");
                    wakeUpSchedule += 1;
                } else {
                    scheduler.append(0 + " ");
                    wakeUpSchedule += 0;
                }
            }
            tmpNode.setWakeUpSchedule(wakeUpSchedule);
            scheduler.append("\n");
        }
        scheduler.append("END" + "\n");
        scheduler.close();
    }

    private static void printWakeUp() {
        System.out.println("WAKE UP SCHEDULE FOR EACH NODE : ");
        for (Node n : vertexList) {
            System.out.println(n.getNodeNumber() + ". " + n.getWakeUpSchedule());
        }
    }

    /**
     * main function for unit testing.
     *
     * @param args
     * @throws java.io.IOException
     */

    public static void main(String[] args) throws IOException {
        generateInputSet(1);
    }

    public static void clearFile() throws IOException {
        file = new File("input_set.txt");
        scheduleFile = new File("input_schedule.txt");
        fileWriter = new FileWriter(file, false);
        writer = new PrintWriter(fileWriter);
        writer.print("");
        scheduleWriter = new FileWriter(scheduleFile, false);
        scheduler = new PrintWriter(scheduleWriter);
        scheduler.print("");
    }

    public static int getNumberOfScheduleBits() {
        return NUMBER_OF_SCHEDULE_BITS;
    }

    public static int getSourceNode() {
        return sourceNode;
    }

    public static ArrayList<Node> getVertexList() {
        return vertexList;
    }

    public static ArrayList<Edge> getEdgeList() {
        return edgeList;
    }

    public static int getNumberOfNode() {
        return numberOfNode;
    }

    public static HashMap<Integer, HashSet<Integer>> getAdjacentList() {
        return graphConnect.getAdjacentList();
    }
}
