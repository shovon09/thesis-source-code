package cse_du.thesis.mtb.input_generator.cover_set_gen;

import java.util.ArrayList;

/**
 * Created by Rifat on 12/14/13.
 */
public class Ci {
    private ArrayList<Integer> activeNodes;
    private ArrayList<Integer> ciNodes = new ArrayList<Integer>();


    public ArrayList<Integer> getActiveNodes() {
        return activeNodes;
    }

    public void setActiveNodes(ArrayList<Integer> activeNodes) {
        this.activeNodes = activeNodes;
    }

    public ArrayList<Integer> getCiNodes() {
        return ciNodes;
    }

    public void setCiNodes(ArrayList<Integer> ciNodes) {
        this.ciNodes = ciNodes;
    }

    public void setCiNode(int ciNode) {
        this.ciNodes.add(ciNode);
    }

    public boolean isNodeInCiNodesOfNode(int node) {
        if (ciNodes.contains(node)) {
            return true;
        } else
            return false;
    }
}
