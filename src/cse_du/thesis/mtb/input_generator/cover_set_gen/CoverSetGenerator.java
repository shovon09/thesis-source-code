package cse_du.thesis.mtb.input_generator.cover_set_gen;

import cse_du.thesis.mtb.input_generator.input_set_gen.domain.Node;

import java.io.PrintWriter;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Shovon_2
 * Date: 11/11/13
 * Time: 10:39 PM
 * To change this template use File | Settings | File Templates.
 */
public class CoverSetGenerator {

    private static ArrayList<Integer> activeNodesAtEachTime;
    private static ArrayList<ArrayList<Integer>> setOfActiveNodes = new ArrayList<ArrayList<Integer>>();
    private static HashMap<String, HashMap<Integer, ArrayList<Integer>>> covListSet = new HashMap<String, HashMap<Integer, ArrayList<Integer>>>();
    private static ArrayList<ArrayList<Integer>> tmpCiSet = new ArrayList<ArrayList<Integer>>();
    private static ArrayList<String> timeSlots = new ArrayList<String>();
    private static HashMap<Integer, ArrayList<Integer>> COV;
    private static HashMap<Integer, ArrayList<Integer>> tmpCOV;
    private static ArrayList<Integer> covList;
    private static ArrayList<Integer> tmpCi;
    private static HashMap<String, Ci> ciSet = new HashMap<String, Ci>();


    public static void generateCoverSet(int scheduleBits, ArrayList<Node> vertexList,
                                        HashMap<Integer, HashSet<Integer>> adjacentList, PrintWriter coverSetFileWriter) {
        initTimeSlot();
        System.out.println();
        System.out.println("Active Nodes at each Time Slot ");

        for (String time : timeSlots) {
            COV = new HashMap<Integer, ArrayList<Integer>>();
            tmpCOV = new HashMap<Integer, ArrayList<Integer>>();
            activeNodesAtEachTime = new ArrayList<Integer>();
            for (Node node : vertexList) {
                String nodeTime = node.getWakeUpSchedule();
                if (time.equals(nodeTime)) {
                    activeNodesAtEachTime.add(node.getNodeNumber());
                }

            }
            setOfActiveNodes.add(activeNodesAtEachTime);
            System.out.println("Active node at time " + time);
            coverSetFileWriter.println(time);
            for (int n : activeNodesAtEachTime) {
                System.out.print(n + " ");
                coverSetFileWriter.print(n + " ");
            }
            coverSetFileWriter.println();
            System.out.println();
            generateCov(activeNodesAtEachTime, adjacentList, time); //Cov(V) generation ... Set of nodes covered by node v at time i.
            printCOV(coverSetFileWriter);
            tmpCOV = (HashMap<Integer, ArrayList<Integer>>) COV.clone();
            generateCi(tmpCOV, activeNodesAtEachTime, coverSetFileWriter, time);
        }

        deleteTimeSlot();
    }

    private static void initTimeSlot() {
        //TODO: For testing purpose manually added this time slots. Must be  automated.
        timeSlots.add("10000");
        timeSlots.add("01000");
        timeSlots.add("00100");
        timeSlots.add("00010");
        timeSlots.add("00001");
    }

    private static void deleteTimeSlot() {
        timeSlots.removeAll(timeSlots);
    }

    private static void generateCov(ArrayList<Integer> activeNodes,
                                    HashMap<Integer, HashSet<Integer>> adjacentList, String time) {
        for (Map.Entry<Integer, HashSet<Integer>> entry : adjacentList.entrySet()) {
            int key = entry.getKey();
            HashSet values = entry.getValue();
            Iterator iterator = values.iterator();
            covList = new ArrayList<Integer>();
            while (iterator.hasNext()) {

                int adjacentNode = (Integer) iterator.next();
                for (int activeNode : activeNodes) {
                    if (adjacentNode == activeNode) {
                        covList.add(activeNode);
                    }
                }
            }
            COV.put(key, covList);
        }
        covListSet.put(time, COV);
    }

    private static void printCOV(PrintWriter covPrinter) {
        System.out.println("PRINTING ALL COV ");

        for (Map.Entry<Integer, ArrayList<Integer>> entry : COV.entrySet()) {
            int key = entry.getKey();
            ArrayList values = entry.getValue();
            Iterator iterator = values.iterator();
            System.out.print(key + " -> ");
            covPrinter.print(key + "->");
            while (iterator.hasNext()) {
                Object obj = iterator.next();
                System.out.print(obj + " ");
                covPrinter.print(obj + " ");
            }
            System.out.println();
            covPrinter.println();
        }
    }

    private static void generateCi(HashMap<Integer, ArrayList<Integer>> covSet, ArrayList<Integer> activeNodes,
                                   PrintWriter ciPrinter, String currentTime) {

        tmpCi = new ArrayList<Integer>();

        Ci ci = new Ci();
        ci.setActiveNodes(activeNodes);

        System.out.println("Ci Generation : ");
        ciPrinter.print("Ci : ");
        while (!activeNodes.isEmpty()) {
            int maxKey = 0;
            int maxKeySize = 0;

            for (Map.Entry<Integer, ArrayList<Integer>> entry : covSet.entrySet()) {

                int key = entry.getKey();
                ArrayList coveredNodesByKey = entry.getValue();
                int tmpSize = coveredNodesByKey.size();
                if (maxKey == 0) {
                    maxKey = key;
                    maxKeySize = tmpSize;
                } else {
                    if (tmpSize > maxKeySize) {
                        maxKey = key;
                        maxKeySize = tmpSize;
                    }
                }

            }
            System.out.print(maxKey + " ");
            ciPrinter.print(maxKey + " ");
            tmpCi.add(maxKey);
            ci.setCiNode(maxKey);// tmpCi Domain.
            ArrayList arrayList = covSet.get(maxKey);
            activeNodes.removeAll(arrayList);
            covSet.remove(maxKey);
            System.out.println();
        }
        ciSet.put(currentTime, ci);
        tmpCiSet.add(tmpCi);
        System.out.println();
        ciPrinter.println();
    }

    public static ArrayList<ArrayList<Integer>> getSetOfActiveNodes() {
        return setOfActiveNodes;
    }

    public static HashMap<String, HashMap<Integer, ArrayList<Integer>>> getCovListSet() {
        return covListSet;
    }

    public static ArrayList<ArrayList<Integer>> getTmpCiSet() {
        return tmpCiSet;
    }

    public static ArrayList<String> getTimeSlots() {
        initTimeSlot();
        return timeSlots;
    }

    public static HashMap<String, Ci> getCiSet() {
        return ciSet;
    }
}
