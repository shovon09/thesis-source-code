package cse_du.thesis.mtb.input_generator;

import cse_du.thesis.mtb.algo_implementation.util.Algorithm;
import cse_du.thesis.mtb.input_generator.cover_set_gen.Ci;
import cse_du.thesis.mtb.input_generator.cover_set_gen.CoverSetGenerator;
import cse_du.thesis.mtb.input_generator.input_set_gen.InputSetGenerator;
import cse_du.thesis.mtb.input_generator.input_set_gen.domain.Edge;
import cse_du.thesis.mtb.input_generator.input_set_gen.domain.Node;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Created with IntelliJ IDEA.
 * User: Shovon_2
 * Date: 11/11/13
 * Time: 11:18 PM
 * To change this template use File | Settings | File Templates.
 */
public class MainController {
    private static final int NUMBER_OF_INPUT_SET = 10;
    private static int numberOfNode;
    private static int sourceNode;
    private static ArrayList<Edge> edgeList;
    private static ArrayList<Node> vertexList;
    private static int scheduleBits;
    private static PrintWriter coverSetWriter;
    private static ArrayList<ArrayList<Integer>> setOfActiveNodes;
    private static HashMap<String, HashMap<Integer, ArrayList<Integer>>> covListSet;
    private static HashMap<String, Ci> ciSet;
    private static HashMap<Integer, HashSet<Integer>> adjacenList;
    private static ArrayList<String> timeSlots;

    /**
     * The task of this function is to manage input generator , coverSet and the main algorithm.
     * This is the entry point of the program.
     *
     * @param args
     */
    public static void main(String[] args) {
        try {
            clearFile();
            coverSetWriter = new PrintWriter(new FileWriter(new File("cover_set.txt"), true));
            int doneInput = 0;
            int prevInput;
            InputSetGenerator.clearFile();
            scheduleBits = InputSetGenerator.getNumberOfScheduleBits();
            while (doneInput < NUMBER_OF_INPUT_SET) {
                prevInput = doneInput;
                doneInput = InputSetGenerator.generateInputSet(doneInput);
                sourceNode = InputSetGenerator.getSourceNode();
                numberOfNode = InputSetGenerator.getNumberOfNode();
                edgeList = InputSetGenerator.getEdgeList();
                vertexList = InputSetGenerator.getVertexList();
                adjacenList = InputSetGenerator.getAdjacentList();
                if (doneInput > prevInput) {
                    coverSetWriter.println("COVER SET");
                    CoverSetGenerator.generateCoverSet(scheduleBits, vertexList, adjacenList, coverSetWriter);
                    coverSetWriter.println("END");

                    setOfActiveNodes = CoverSetGenerator.getSetOfActiveNodes();
                    covListSet = CoverSetGenerator.getCovListSet();
                    ciSet = CoverSetGenerator.getCiSet();
                    timeSlots = CoverSetGenerator.getTimeSlots();
                    Algorithm.runAlgorithm(sourceNode, timeSlots, vertexList, edgeList, adjacenList,
                            setOfActiveNodes, covListSet, ciSet);
                }

            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            coverSetWriter.close();
        }
    }

    public static void clearFile() throws IOException {
        coverSetWriter = new PrintWriter(new FileWriter(new File("cover_set.txt"), false));
        coverSetWriter.write("");
    }
}
