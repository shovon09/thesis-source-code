package cse_du.thesis.mtb.algo_implementation.util;

import cse_du.thesis.mtb.input_generator.cover_set_gen.Ci;
import cse_du.thesis.mtb.input_generator.input_set_gen.domain.Edge;
import cse_du.thesis.mtb.input_generator.input_set_gen.domain.Node;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Stack;

/**
 * Created by Shovon on 12/12/13.
 */
public class Algorithm {
    private static ArrayList<Integer> coveredNodes;
    private static ArrayList<Integer> coveringNodes;
    private static HashMap<Integer, HashSet<Integer>> broadCastingGraph;
    private static ArrayList<Integer> Tbcast;
    private static Stack<Integer> nodeStack;
    private static int currentNode;
    private static Ci ciSetOfCurrentTime;
    private static ArrayList<Integer> tmpVertex;

    public static void runAlgorithm(int sourceNode, ArrayList<String> timeSlots, ArrayList<Node> vertexList,
                                    ArrayList<Edge> edgeList,
                                    HashMap<Integer, HashSet<Integer>> adjacentList,
                                    ArrayList<ArrayList<Integer>> setOfActiveNodes,
                                    HashMap<String, HashMap<Integer, ArrayList<Integer>>> covListSet,
                                    HashMap<String, Ci> ciSet) {

        tmpVertex = new ArrayList<Integer>();
        for (Node node : vertexList) {
            tmpVertex.add(node.getNodeNumber());
        }
        nodeStack = new Stack<Integer>();
        coveredNodes = new ArrayList<Integer>();
        coveringNodes = new ArrayList<Integer>();
        broadCastingGraph = new HashMap<Integer, HashSet<Integer>>();
        Tbcast = new ArrayList<Integer>();

        nodeStack.push(sourceNode);
        coveredNodes.add(sourceNode);
        coveringNodes.add(sourceNode);

        System.out.println("BROAD CASTING GRAPH");
        while (!tmpVertex.isEmpty()) {

            while (!nodeStack.isEmpty()) {
                currentNode = nodeStack.pop();
                for (int i = 0; i < timeSlots.size(); i++) {
                    String currentTime = timeSlots.get(i);


                    ArrayList<Integer> activeNodes = setOfActiveNodes.get(i); //Getting the active nodes of time i.
                    HashMap<Integer, ArrayList<Integer>> covAtCurrentTime = covListSet.get(currentTime); //Getting the cov list of time i.
                    ciSetOfCurrentTime = ciSet.get(currentTime); //Getting the Ci set of current time.

                    connectCurrentNodeToCi(currentNode, ciSetOfCurrentTime, adjacentList, covAtCurrentTime, currentTime);// connect the current node to ci.

                }
            }
            System.out.println("COVERING NODES " + coveringNodes);
            tmpVertex.removeAll(coveringNodes);
            if (!tmpVertex.isEmpty()) {
                //TODO: DO as the algorithm was designed.
                for (int node : tmpVertex) {
                    for (String time : timeSlots) {
                        HashMap<Integer, ArrayList<Integer>> covList = covListSet.get(time);
                        ArrayList<Integer> covOfNode = covList.get(node);
                        for (int n : covOfNode) {
                            if (coveringNodes.contains(n)) {
                                connectXToY(n, node);
                                nodeStack.push(node);
                            }
                        }
                    }
                }
            }
        }

    }

    private static ArrayList<Integer> getCov(int keyNode, HashMap<Integer, ArrayList<Integer>> covAtCurrentTime) {
        return covAtCurrentTime.get(keyNode);
    }

    private static void connectCurrentNodeToCi(int currentNode, Ci ci, HashMap<Integer, HashSet<Integer>> adjacentList,
                                               HashMap<Integer, ArrayList<Integer>> covAtCurrentTime, String currentTime) {

        HashSet<Integer> adjacentNodesOfCurrentNode = adjacentList.get(currentNode);

        for (int ciNode : ci.getCiNodes()) {
            ArrayList<Integer> covListOfCiNode = covAtCurrentTime.get(ciNode);

            if (adjacentNodesOfCurrentNode.contains(ciNode)) {

                if (!coveringNodes.contains(ciNode)) {
                    coveringNodes.add(ciNode);
                    nodeStack.push(ciNode);
                    connectXToY(currentNode, ciNode,currentTime);
                }
                for (int node : covListOfCiNode) {
                    if (!coveringNodes.contains(node)) {
                        coveringNodes.add(node);
                        nodeStack.push(node);
                        connectXToY(ciNode, node,currentTime);
                    }
                }
            } else if (ciNode == currentNode) {
                // connect ( currentNode-> Covi(currentNode) );
                for (int node : covListOfCiNode) {
                    if (!coveringNodes.contains(node)) {
                        coveringNodes.add(node);
                        nodeStack.push(node);
                        connectXToY(ciNode, node,currentTime);
                    }
                }
            }
        }
    }

    private static void connectXToY(int x, int y, String currentTime) {
        System.out.println("TIME : " + currentTime);
        //TODO: Implement this
        System.out.println(x + " --> " + y);
    }

    private static void connectXToY(int x, int y) {
        //TODO: Implement this
        System.out.println(x + " --> " + y);
    }
}
