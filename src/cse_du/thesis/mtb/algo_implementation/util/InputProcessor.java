package cse_du.thesis.mtb.algo_implementation.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: Shovon
 * Date: 11/27/13
 * Time: 4:02 PM
 * To change this template use File | Settings | File Templates.
 */
public class InputProcessor {
    private static File file;
    private static Scanner fileScanner;

    public static void processInputSet() {
        try {
            fileScanner = new Scanner(new BufferedReader(new FileReader("input_set.txt")));
            while (fileScanner.hasNext()) {
                // TODO : for checking purpose , delete this. and process input
//                System.out.println(fileScanner.nextLine());

            }
        } catch (FileNotFoundException e) {

            e.printStackTrace();
        } finally {
            fileScanner.close();
        }


    }

    public static void processInputSchedule() {

        try {
            fileScanner = new Scanner(new BufferedReader(new FileReader("input_schedule.txt")));
            while (fileScanner.hasNext()) {
                // TODO : for checking purpose , delete this. and process input
                System.out.println(fileScanner.nextLine());
            }
        } catch (FileNotFoundException e) {

            e.printStackTrace();
        } finally {
            fileScanner.close();
        }
    }

    public static void processCi() {

    }

    public static void processCOV() {

    }
}
