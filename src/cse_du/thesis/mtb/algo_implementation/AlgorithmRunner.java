package cse_du.thesis.mtb.algo_implementation;

import cse_du.thesis.mtb.algo_implementation.util.InputProcessor;

/**
 * Created with IntelliJ IDEA.
 * User: Shovon
 * Date: 11/27/13
 * Time: 12:52 PM
 * To change this template use File | Settings | File Templates.
 */
public class AlgorithmRunner {

    /**
     * For unit testing
     *
     * @param args
     */
    public static void main(String[] args) {
        InputProcessor.processInputSet();
        InputProcessor.processInputSchedule();

    }
}
